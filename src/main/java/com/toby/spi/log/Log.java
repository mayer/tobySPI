package com.toby.spi.log;

import com.toby.spi.annotation.TobySPI;

@TobySPI("feichaoLog")
public interface Log {

    void say();

}
