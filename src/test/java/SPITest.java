import com.toby.spi.loader.TobyExtensionLoader;
import com.toby.spi.log.Log;
import org.junit.Test;

public class SPITest {

    @Test
    public void test() throws Exception {
        //获取默认实现类
        Log defaultExtension = TobyExtensionLoader.
                getExtensionLoader(Log.class).
                getDefaultExtension();
        defaultExtension.say();

        //指定特定的实现类,例如配置的tobyLog
        Log tobyLog = TobyExtensionLoader.
                getExtensionLoader(Log.class).
                getExtension("tobyLog");
        tobyLog.say();

    }

}
